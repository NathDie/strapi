module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'cd3eca384e21a30fbdc92465d38dbf30'),
  },
});
